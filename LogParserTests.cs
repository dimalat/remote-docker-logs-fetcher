using System;
using System.Collections.Generic;
using Xunit;

namespace dimalat.RemoteDockerLogsFetcher
{
    public class LogParserTests
    {
        [Fact]
        public void CanSplitLogsToListOfObjects()
        {
            //Given
            var logs = GetLogExample();
            var logParser = new LogParser();

            //When
            List<Log> list = logParser.Parse(logs);

            //Then
            Assert.NotEmpty(list);
            Assert.True(list.Count == 4);
            Assert.NotEqual(default(LogType), list[0].Type);
        }

        private string GetLogExample()
        {
            return @"docker logs botyao_bff_1
            u001b[40m\u001b[1m\u001b[33mwarn\u001b[39m\u001b[22m\u001b[49m: Microsoft.AspNetCore.DataProtection.Repositories.FileSystemXmlRepository[60]
            Storing keys in a directory '/root/.aspnet/DataProtection-Keys' that may not be persisted outside of the container. Protected data will be 
            unavailable when container is destroyed.\r\n\u001b[40m\u001b[32minfo\u001b[39m\u001b[22m\u001b[49m: Microsoft.AspNetCore.DataProtection.K";
        }
    }
}