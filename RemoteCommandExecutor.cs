using System;
using System.IO;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading;
using System.Threading.Tasks;
using Renci.SshNet;
using Renci.SshNet.Common;

namespace dimalat.RemoteDockerLogsFetcher
{
    public class RemoteCommandExecutor : IDisposable
    {
        private readonly string ip;
        private readonly string user;
        private readonly string pass;
        private SshClient session;


        public RemoteCommandExecutor(string ip, string user, string pass)
        {
            this.pass = pass;
            this.user = user;
            this.ip = ip;
        }

        public async Task<string> Execute(string command)
        {

            if (session == null)
            {
                session = new SshClient(ip, 22, user, pass);
                session.Connect();
            }
            var sc = session.CreateCommand(command);

            var output = await Task<string>.Factory.FromAsync(sc.BeginExecute(), sc.EndExecute);
            var withoutControlCodes = Regex.Replace(output, "\\u001B\\[[0-9]{2}m", "");
            return withoutControlCodes;
            // return new string(withoutControlCodes.Where(c => !char.IsControl(c)).ToArray());
        }

        public void Dispose()
        {
            session.Dispose();
        }
    }
}