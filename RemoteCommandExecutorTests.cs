using System;
using System.Threading.Tasks;
using Xunit;

namespace dimalat.RemoteDockerLogsFetcher.tests
{
    public class RemoteCommandExecutorTests
    {

        [Fact]
        public async Task Execute_Command_returnsOutput()
        {
            //Given
            using var remoteCommandExecutor = Create();

            //When
            var commandOutput = await remoteCommandExecutor.Execute("docker ps");

            //Then
            Assert.False(string.IsNullOrEmpty(commandOutput));
        }

        [Fact]
        public async Task Execute_TwoCommands_OutputDoesNotOverlap()
        {
            //Given
            using var remoteCommandExecutor = Create();

            //When
            var pwdOut = await remoteCommandExecutor.Execute("pwd");
            var whoamiOut = await remoteCommandExecutor.Execute("whoami");

            //Then
            Assert.False(whoamiOut.Contains(pwdOut));
        }

        public static RemoteCommandExecutor Create()
        {
            var ip = Environment.GetEnvironmentVariable("ecom_server");
            var user = Environment.GetEnvironmentVariable("ecom_user");
            var pass = Environment.GetEnvironmentVariable("ecom_pass");

            return new RemoteCommandExecutor(ip, user, pass);
        }
    }
}