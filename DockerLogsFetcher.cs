using System;
using System.Threading.Tasks;

namespace dimalat.RemoteDockerLogsFetcher
{
    public class DockerLogsFetcher
    {
        private RemoteCommandExecutor ssh;

        public DockerLogsFetcher(RemoteCommandExecutor commandExecutor)
        {
            this.ssh = commandExecutor;
        }

        public async Task<string> GetAllLogs(string containerName)
        {
            var logs = await ssh.Execute($"docker logs {containerName}");

            ThrowIfNoContainer(logs);

            return logs;
        }

        public async Task<string> Get(string containerName, int logCount)
        {
            if(logCount < 1)
                return string.Empty;

            var logs = await ssh.Execute($"docker logs --tail {logCount} {containerName}");

            ThrowIfNoContainer(logs);

            return logs;
        }

        private static void ThrowIfNoContainer(string logs)
        {
            if (logs.Contains("Error: No such container"))
            {
                throw new Exception("No such container in process.");
            }
        }
    }
}