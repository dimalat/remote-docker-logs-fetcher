using System;
using System.Collections.Generic;
using System.Linq;

namespace dimalat.RemoteDockerLogsFetcher
{
    public class LogParser
    {
        public LogParser()
        {
        }

        public List<Log> Parse(string logs)
        {
            return logs.Split(
                    new[] { Environment.NewLine },
                    StringSplitOptions.None
                ).Select(x => new Log
                {
                    Type = ParseType(x),
                    Message = x
                }).ToList();
        }

        private DateTime ParseDateTime(string x)
        {
            throw new NotImplementedException();
        }

        private LogType ParseType(string x)
        {
            if (
                x.Contains("Exception") ||
                x.Contains("error", StringComparison.InvariantCultureIgnoreCase) ||
                x.Contains("fail", StringComparison.InvariantCultureIgnoreCase))
                return LogType.Exception;

            if (x.Contains("Info", StringComparison.InvariantCultureIgnoreCase))
                return LogType.Info;

            return LogType.Misc;
        }
    }

    public enum LogType
    {
        Exception,
        Info,
        Misc
    }
}