using System;
using System.Linq;
using System.Threading.Tasks;
using Xunit;

namespace dimalat.RemoteDockerLogsFetcher.tests
{
    public class DockerLogsFetcherTests
    {
        [Fact]
        public async Task MustGetAllLogsByConainerName()
        {
            //Given
            var commandExecutor = RemoteCommandExecutorTests.Create();
            var fetcher = new DockerLogsFetcher(commandExecutor);

            //When
            var logs = await fetcher.GetAllLogs("botyao_bff_1");

            //Then
            Assert.False(string.IsNullOrEmpty(logs));
        }

        [Theory]
        [InlineData(1)]
        [InlineData(50)]
        public async Task GetLastNLogs(int logsCount)
        {
            //Given
            var commandExecutor = RemoteCommandExecutorTests.Create();
            var fetcher = new DockerLogsFetcher(commandExecutor);

            //When
            var logs = await fetcher.Get("botyao_bff_1", logsCount);

            //Then
            var actualCount = logs.Split(
                    new[] { Environment.NewLine },
                    StringSplitOptions.None).Count();

            Assert.Equal(logsCount+1, actualCount);
        }

        [Fact]
        public async Task GetLast0Logs_ReturnsEmptyString()
        {
            //Given
            var commandExecutor = RemoteCommandExecutorTests.Create();
            var fetcher = new DockerLogsFetcher(commandExecutor);

            //When
            var logs = await fetcher.Get("botyao_bff_1", 0);

            //Then
            Assert.True(string.IsNullOrEmpty(logs));
        }
    }
}