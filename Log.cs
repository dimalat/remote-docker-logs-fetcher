using System;

namespace dimalat.RemoteDockerLogsFetcher
{
    public class Log
    {
        public LogType Type { get; set; }
        public string Message { get; set; }
    }
}